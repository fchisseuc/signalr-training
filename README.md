# SignalR-Training

SignalR is a Library to add any sort of "real-time" web functionality to your ASP.NET application.

“Real-time” web functionality is the ability to have server-side code push content to the connected clients as it happens, in real-time.

The library includes :
* server-side (ASP.Net, ASP.Net Core) : sends asynchronous notifications to client-side
* client-side (javascript, typescript) : connects to server-side to receive notifications

Good candidates :
* any time a user refreshes a web page to see new data
* apps that require notifications (chat, email, games, social networks, travel alerts …), dashboards and monitoring apps (instant sales updates …)
* apps that require high frequency updates from the server (gaming, social networks, voting, auction, maps, GPS apps …)

Tutorial  
https://docs.microsoft.com/fr-fr/aspnet/core/tutorials/signalr?view=aspnetcore-3.0&tabs=visual-studio
https://codingblast.com/asp-net-core-signalr-chat-angular/
https://www.c-sharpcorner.com/article/real-time-angular-8-application-with-signalr-and-cosmos-db/
https://blogs.infinitesquare.com/posts/web/integration-de-signalr-dans-une-application-aspnet-core-avec-angular-5