import { Component } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  private hubConnection: HubConnection;
  user = '';
  message = '';
  messages: string[] = [];

  ngOnInit() {
    this.user = window.prompt('Your name:', 'Firmin');

    // create connection
    this.createConnection();

    // start connection
    this.startConnection();

    // register on server event
    this.registerOnServerEvents();
  }

  private createConnection() {
    this.hubConnection = new HubConnectionBuilder()
    .withUrl('http://localhost:3000/chat')
    .build();
  }

  private startConnection(): void {
    this.hubConnection
      .start()
      .then(() => console.log('Connection started!'))
      .catch(err => console.log('Error while establishing connection :('));
    this.hubConnection.serverTimeoutInMilliseconds =  300000; // 300 seconds => 5 min
  }

  private registerOnServerEvents(): void {
    this.hubConnection.on('eventMessageReceived', (sender: string, receivedMessage: string) => {
      // console.log('user = ' + username);
      // console.log('received message = ' + receivedMessage);
      const text = `${sender}: ${receivedMessage}`;
      this.messages.push(text);
    });
  }

  public sendMessage(): void {
    // invoke a server hub method
    this.hubConnection
      .invoke('SendMessageToAll', this.user, this.message)
      .catch(err => console.error(err));
    this.clearMessage();
  }

  public clearMessage(): void {
    this.message = '';
  }
}
