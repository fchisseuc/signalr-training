﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SignalRChatApi.DTO
{
    public class ChatMessage
    {
        public ChatMessage()
        {
        }

        [Required]
        public string User
        {
            get;
            set;
        }

        [Required]
        public string Message
        {
            get;
            set;
        }
    }
}
