﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalRChatApi.Hubs
{
    /// <summary>
    /// manages connections, groups, and messaging
    /// </summary>
    public class ChatHub:Hub
    {
        public const string EVENT_MESSAGE_RECEIVED = "eventMessageReceived";

        /// <summary>
        /// method called by a connected client to send a message to all clients
        /// => will invoke an event on the connection to warn the subscribed clients that they received a message
        /// </summary>
        /// <param name="user">User that sends the message</param>
        /// <param name="message">message sent</param>
        /// <returns></returns>
        public async Task SendMessageToAll(string user, string message)
        {
            await Clients.All.SendAsync(EVENT_MESSAGE_RECEIVED, user, message);
        }
    }
}
